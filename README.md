# Linux Kernel reports

A set of scripts to generate reports about the kernel. So far we have:

* `contributions-stats.py`: compute stats about Authors, Mantainer committed patches,
patches sent on behalf of others and the usual tags like    Reviewed-by, Acked-by, Tested-by and Reported-by for a given
list of commits.
* `generate-html-tree.py`: create a html file with tree navigation and link to the the commits for a given list
of git commits

## Installing

On a Python 3 environment run:

```
pip install -r requirements.txt
```

One of the python packages to install is `pygit2`, which depends on `libgit2 1.0.x`, if your
system doesn't have a package for it, install it manually on a local prefix and follow pygit2
docs instructions.

## Using

### Contributions stats

To use it run the below from within your kernel directory:

```
git log --pretty='format:%h,"%s",,' <revision range> | <path to>/contribution-stats.py
```

Argument options:

* `-h, --help`: show help
* `-s, --summary`: sort output according to the number of commits instead of alphabetic order
* `-n, --numbered`: suppress commit description and provide a commit count summary only
* `-e, --email`: show the email address of each contributor
* `-o FILE, --output FILE`: output result to a file instead of stdout
* `--html FILE`: output as html with link to commits

### Generating HTML tree view

The `stdin` input is a csv list with the following format:

`hash,commit subject, comments, status`

*comments* and *status* are optional.

```
git log --pretty='format:%h,"%s",,' <revision range> | ../kernel-scripts/generate-html-tree.py
```

or pass your csv file as as stdin:

```
cat commits.csv | ../kernel-scripts/generate-html-tree.py
```

Argument options:

* `-h, --help`: show help
* `-o, --output`: basename for the json and HTML output files (default is 'tree')
* `-c, --cmdline`: the cmd line used to generate the list of patches
* `-t, --title`: title for the HTML report

### scripts/ folder

The `scripts/` folder has some ready to use scripts to generate the analysis for some trees.
